﻿namespace PedidosApp.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using PedidosApp.Models;
    using System.Windows.Input;
    using Xamarin.Forms;
    using PedidosApp.Views;

    public class PedidoItemViewModel : Pedido
    {
        #region Comandos
        public ICommand SelectPedidoCommand
        {
            get
            {
                return new RelayCommand(SelectPedido);
            }
        }

        private async void SelectPedido()
        {
            MainViewModel.GetInstance().Pedido = new PedidoViewModel(this);
            await Application.Current.MainPage.Navigation.PushAsync(new PedidoTabbedPage());
        }
        #endregion
    }
}
