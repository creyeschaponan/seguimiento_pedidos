﻿namespace PedidosApp.ViewModels
{
    using PedidosApp.Models;
    using System.Collections.Generic;

    public class MainViewModel
    {
        #region Propiedades
        public List<Pedido> PedidosList
        {
            get;
            set;
        }

        public TokenResponse Token { get; set; }
        #endregion

        #region ViewModels
        public LoginViewModel Login
        {
            get;
            set;
        }

        public RegisterViewModel Register
        {
            get;
            set;
        }

        public ForgotPasswordViewModel ForgotPassword
        {
            get;
            set;
        }

        public PedidosViewModel Pedidos
        {
            get;
            set;
        }

        public PedidoViewModel Pedido
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;
            this.Login = new LoginViewModel();
        }
        #endregion

        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion
    }
}
