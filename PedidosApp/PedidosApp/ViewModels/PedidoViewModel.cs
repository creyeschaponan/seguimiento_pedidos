﻿namespace PedidosApp.ViewModels
{
    using PedidosApp.Models;
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;

    public class PedidoViewModel : BaseViewModel
    {
        #region Atributos
        private ObservableCollection<PedidoProducto> pedidoProductos;
        #endregion

        #region Propiedades
        public Pedido Pedido
        {
            get;
            set;
        }

        public ObservableCollection<PedidoProducto> PedidoProductos
        {
            get { return this.pedidoProductos; }
            set { this.SetValue(ref this.pedidoProductos, value); }
        }

        #endregion


        #region Constructores
        public PedidoViewModel(Pedido pedido)
        {
            this.Pedido = pedido;
            this.PedidoProductos = new ObservableCollection<PedidoProducto>(this.Pedido.PedidoProducto);
        }

        #endregion

        #region Metodos
      
        #endregion
    }
}
