﻿namespace PedidosApp.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using PedidosApp.Models;
    using PedidosApp.Services;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class PedidosViewModel : BaseViewModel
    {

        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private ObservableCollection<PedidoItemViewModel> pedidos;
        private bool isRefreshing;
        private string filter;
  
        #endregion

        #region Properties
        public ObservableCollection<PedidoItemViewModel> Pedidos
        {
            get { return this.pedidos; }
            set { SetValue(ref this.pedidos, value); }
        }
        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }
        public string Filter
        {
            get { return this.filter; }
            set {
                SetValue(ref this.filter, value);
                this.Search();
            }
        }
        #endregion

        #region Constructors
        public PedidosViewModel()
        {
            this.apiService = new ApiService();
            this.LoadPedidos();
        }
        #endregion

        #region Methods

        private async void LoadPedidos()
        {
            this.IsRefreshing = true;
            var connection = await this.apiService.CheckConnection();

            if(!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                  "Error",
                   connection.Message,
                  "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            var token = MainViewModel.GetInstance().Token.AccessToken;

            var response = await this.apiService.GetList<Pedido>(
                "http://sds.envasessannicolas.pe",
                "/api",
                "/cliente/pedido/listado", "Bearer", token
               );

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                     response.Message,
                    "Accept");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }


            MainViewModel.GetInstance().PedidosList = (List<Pedido>)response.Result;

            this.Pedidos = new ObservableCollection<PedidoItemViewModel>(
                this.ToPedidoItemViewModel());
            this.IsRefreshing = false;


        }
        #endregion

        #region Metodos

        private IEnumerable<PedidoItemViewModel> ToPedidoItemViewModel()
        {
            return MainViewModel.GetInstance().PedidosList.Select(p => new PedidoItemViewModel
            {
                Estado = p.Estado,
                EstadoColor = p.EstadoColor,
                Fecha_fin = p.Fecha_fin,
                Fecha_inicio = p.Fecha_inicio,
                Id_pedido = p.Id_pedido,
                Id_persona = p.Id_persona,
                Name = p.Name,
                Nro_orden = p.Nro_orden,
                StringEstado = p.StringEstado,
                Ver_pedido = p.Ver_pedido,
                PedidoProducto = p.PedidoProducto
            });
        }

        #endregion

        #region Comandos
        public  ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadPedidos);
            }

        }

        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);
            }
        }


        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                this.Pedidos = new ObservableCollection<PedidoItemViewModel>(
                                this.ToPedidoItemViewModel());
            }
            else
            {
                this.Pedidos = new ObservableCollection<PedidoItemViewModel>(
                     this.ToPedidoItemViewModel().Where( //linquiu 
                        p => p.Nro_orden.ToLower().Contains(this.Filter.ToLower()) ||
                             p.Fecha_inicio.ToLower().Contains(this.Filter.ToLower()) ||
                             p.Fecha_fin.ToLower().Contains(this.Filter.ToLower()) ||
                             p.StringEstado.ToLower().Contains(this.Filter.ToLower())));//Expreción landa
            }
        }


        #endregion


    }
}
