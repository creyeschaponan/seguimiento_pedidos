﻿namespace PedidosApp.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using PedidosApp.Models;
    using PedidosApp.Services;
    using PedidosApp.Views;
    using System;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class RegisterViewModel : BaseViewModel
    {
        #region Servicios
        private ApiService apiService;
        #endregion

        #region Attributes
        private string name;
        private string email;
        private string documento;
        private string password;
        private string confirm_password;
   
        private bool isRunning;
        #endregion

        #region Properties
        public string Name
        {
            get { return name; }
            set { SetValue(ref name, value); }
        }
        public string Documento
        {
            get { return documento; }
            set { SetValue(ref documento, value); }
        }
        public string Email
        {
            get { return email; }
            set { SetValue(ref email, value); }
        }
        public string Password
        {
            get { return password; }
            set { SetValue(ref password, value); }
        }
        public string Confirm_password
        {
            get { return confirm_password; }
            set { SetValue(ref confirm_password, value); }
        }

        public bool IsRunning
        {
            get { return isRunning; }
            set { SetValue(ref isRunning, value); }
        }

        #endregion

        public RegisterViewModel()
        {
            this.apiService = new ApiService();
        }

        #region Commands
        public ICommand SaveRegisterCommand
        {
            get
            {
                return new RelayCommand(SaveRegister);
            }
        }

        private async void SaveRegister()
        {
            if (string.IsNullOrEmpty(this.Name))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an Name",
                    "Accept");
                return;
            }
            if (string.IsNullOrEmpty(this.Documento))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an Document",
                    "Accept");
                return;
            }
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an Email",
                    "Accept");
                return;
            }
            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an Password",
                    "Accept");
                return;
            }
            if (string.IsNullOrEmpty(this.Confirm_password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an Confirm password",
                    "Accept");
                return;
            }

            this.IsRunning = true;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRunning = false;
                await Application.Current.MainPage.DisplayAlert(
                   "Error",
                   connection.Message,
                   "Accept");
                return;
            }

            var register = new Register();
            register.Name = Name;
            register.Documento = Documento;
            register.Email = Email;
            register.Password = Password;
            register.Confirm_password = Confirm_password;

            var token = await this.apiService.PostToken("http://sds.envasessannicolas.pe/api/register",register);

            if (token == null)
            {
                this.IsRunning = false;

                await Application.Current.MainPage.DisplayAlert(
                   "Error",
                   "A sucedido un error, porfavor intentar más tarde.",
                   "Accept");
                return;
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                this.IsRunning = false;

                await Application.Current.MainPage.DisplayAlert(
                   "Error",
                   "El Cliente no esta registrado en nuestra base de datos.",
                   "Accept");
                this.Password = string.Empty;
                return;
            }


            await Application.Current.MainPage.DisplayAlert(
                    "Alerta",
                    "Cliente Registrado Correctamente",
                    "Accept");

            this.IsRunning = false;

            this.Email = string.Empty;
            this.Name = string.Empty;
            this.Documento = string.Empty;
            this.Password = string.Empty;
            this.Confirm_password = string.Empty;

            await Application.Current.MainPage.Navigation.PopAsync();
        }

        #endregion
    }
}
