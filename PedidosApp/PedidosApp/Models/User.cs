﻿namespace PedidosApp.Models
{
    using Newtonsoft.Json;

    public class User
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        [JsonProperty(PropertyName = "id_persona")]
        public string Id_persona { get; set; }

        [JsonProperty(PropertyName = "tipo")]
        public string Tipo { get; set; }

    }

    public class UserObject
    {
        [JsonProperty(PropertyName = "access_token")]
        public string Access_token { get; set; }

        [JsonProperty(PropertyName = "user")]
        public User User { get; set; }
    }
}
