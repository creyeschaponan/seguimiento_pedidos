﻿using Newtonsoft.Json;

namespace PedidosApp.Models
{
    public class PedidoProducto
    {
        [JsonProperty(PropertyName = "id_pedido_producto")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "id_pedido")]
        public int IdPedido { get; set; }

        [JsonProperty(PropertyName = "id_producto")]
        public int IdProducto { get; set; }

        [JsonProperty(PropertyName = "codigo_producto")]
        public string CodigoProducto { get; set; }

        [JsonProperty(PropertyName = "nombre_producto")]
        public string NombreProducto { get; set; }

        [JsonProperty(PropertyName = "cantidad_producto")]
        public int CantidadProducto { get; set; }

        [JsonProperty(PropertyName = "estado_producto")]
        public string EstadoProducto { get; set; }

        [JsonProperty(PropertyName = "cantidad_por_entregar")]
        public string CantidadPorEntregar { get; set; }

        [JsonProperty(PropertyName = "string_estado")]
        public string StringEstado { get; set; }

        [JsonProperty(PropertyName = "estado_color")]
        public string EstadoColor { get; set; }
    }
}
