﻿namespace PedidosApp.Models
{

    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class Pedido
    {
        [JsonProperty(PropertyName = "id_pedido")]
        public int Id_pedido { get; set; }

        [JsonProperty(PropertyName = "nro_orden")]
        public string Nro_orden { get; set; }

        [JsonProperty(PropertyName = "id_persona")]
        public int Id_persona { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "fecha_inicio")]
        public string Fecha_inicio { get; set; }

        [JsonProperty(PropertyName = "fecha_fin")]
        public string Fecha_fin { get; set; }

        [JsonProperty(PropertyName = "estado")]
        public string Estado { get; set; }

        [JsonProperty(PropertyName = "ver_pedido")]
        public string Ver_pedido { get; set; }

        [JsonProperty(PropertyName = "string_estado")]
        public string StringEstado { get; set; }

        [JsonProperty(PropertyName = "estado_color")]
        public string EstadoColor { get; set; }

        [JsonProperty(PropertyName = "pedido_producto")]
        public List<PedidoProducto> PedidoProducto { get; set; }
    }
}
